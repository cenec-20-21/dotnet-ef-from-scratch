﻿using System;
using System.Collections.Generic;

namespace Main.Entities
{
    public class Alumno
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public List<Matricula> Matriculas { get; set; }
    }
}