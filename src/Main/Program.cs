﻿using Main.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var miDbContext = new MiDbContext();
            miDbContext.Alumnos.Add(new Alumno
            {
                Nombre = "ee",
                Apellido = "Rogelio",
                FechaNacimiento = DateTime.UtcNow
            });
            miDbContext.SaveChanges();

            var foo = miDbContext.Alumnos.Where(x => x.Nombre == "ee");
        }
    }
}