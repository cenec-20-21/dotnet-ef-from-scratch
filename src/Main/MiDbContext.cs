﻿using Main.Entities;
using Microsoft.EntityFrameworkCore;

namespace Main
{
    public class MiDbContext
        : DbContext
    {
        public DbSet<Alumno> Alumnos { get; set; }
        //public DbSet<Profesor> Profesores { get; set; }
        public DbSet<Asignatura> Asignaturas { get; set; }
        public DbSet<Matricula> Matriculas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                "Server=localhost;Database=LaBuena;user=sa;password=Password12!");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // modelBuilder
            //     .Entity<Asignatura>()
            //     .HasOne<Profesor>(x => x.Profesor)
            //     .WithOne(x => x.Asignatura);
            //
            // modelBuilder
            //     .Entity<Profesor>()
            //     .HasOne<Asignatura>(x => x.Asignatura)
            //     .WithOne(x => x.Profesor);
        }
    }
}